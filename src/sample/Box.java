package sample;

import javafx.scene.shape.Rectangle;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class Box extends Rectangle {

    private PVector location;
    private PVector velocity;
    private PVector acceleration;

    Box(PVector location, double w, double h) {
        super(w, h);
        setX(location.x);
        setY(location.y);
        this.location = location;
        this.velocity = new PVector(0,0);
        acceleration = new PVector(0,0);
    }

    void setVelocity(PVector velocity) { this.velocity = new PVector(velocity.x, velocity.y); }

    void setAcceleration(PVector acceleration) {
        this.velocity = new PVector(acceleration.x, acceleration.y);
    }

    PVector getVelocity() {
        return new PVector(velocity.x, velocity.y);
    }

    public Rectangle getBounds() { return new Rectangle(location.x, location.y, getWidth(), getHeight()); }

    void move() {
        velocity = velocity.add(acceleration);
        location = location.add(velocity);
        acceleration.mult(0);
    }

    void update() {
        setX(location.x);
        setY(location.y);
    }

    void checkEdges() {
        if (location.x + getWidth() >= 1024) {
            location.x = 1024 - getWidth();
            velocity.x *= -1;
        } else if (location.x <= 0) {
            velocity.x *= -1;
            location.x = 0;
        }
        if (location.y + getHeight() >= 768) {
            location.y = 768 - getHeight();
            velocity.y *= -1;
        } else if (location.y <= 0) {
            velocity.y *= -1;
            location.y = 0;
        }
    }

    void draw(GraphicsContext gc) {
        gc.setStroke(Color.GREEN);
        gc.strokeRect(location.x, location.y, getWidth(), getHeight());
    }
}
