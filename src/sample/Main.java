package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.scene.shape.Rectangle;

public class Main extends Application {

    private BorderPane root = new BorderPane();
    private Pane center = new Pane();
    private Canvas part;
    private Canvas gravCanvas = new Canvas(1024, 768);
    private GraphicsContext gc;
    private Random random;
    private List<Box> allObjects = new ArrayList<>();
    private ArrayList<Box> boxes = new ArrayList<>();
    private Quadtree quadtree;
    private Point point;
    private Entity entity;
    private Box box;
    private boolean ranOnce = true;

    private final long[] frameTimes = new long[100];
    private int frameTimeIndex = 0 ;
    private boolean arrayFilled = false ;
    double frameRate;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(createContent());
        stage.setTitle("Demo");
        stage.setResizable(false);
        stage.setFullScreen(false);
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }

        };

        AnimationTimer frameRateMeter = new AnimationTimer() {
            Label label = new Label();

            // https://stackoverflow.com/questions/28287398/what-is-the-preferred-way-of-getting-the-frame-rate-of-a-javafx-application
            @Override
            public void handle(long now) {
                long oldFrameTime = frameTimes[frameTimeIndex] ;
                frameTimes[frameTimeIndex] = now ;
                frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length ;
                if (frameTimeIndex == 0) {
                    arrayFilled = true ;
                }
                if (arrayFilled) {
                    long elapsedNanos = now - oldFrameTime ;
                    long elapsedNanosPerFrame = elapsedNanos / frameTimes.length ;
                    frameRate = 1_000_000_000.0 / elapsedNanosPerFrame ;
                    label.setText(String.format("Current frame rate: %.3f", frameRate));
                }
            }
        };

        frameRateMeter.start();
        timer.start();
        stage.setScene(scene);
        stage.show();
    }

    public Parent createContent() {
        root.setPrefSize(1024, 768);
        center.setPrefSize(1024, 768);
        part = new Canvas(1024, 768);
        gc = part.getGraphicsContext2D();
        part.setTranslateX(0);
        part.setTranslateY(0);
        root.setCenter(part);
        root.setCenter(center); // main center Pane to add to.
        root.getChildren().add(gravCanvas); // canvas for sprites or what ever else.

        return root;
    }

    void update() {
        gravCanvas.getGraphicsContext2D().clearRect(0, 0, gravCanvas.getWidth(), gravCanvas.getHeight()); // clear the canvas
        render();

        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getButton().equals(MouseButton.PRIMARY)) {
                    //addObjects();
                }
                if (e.getButton().equals(MouseButton.SECONDARY)) {
                    //boxes.clear();
                }
            }
        });
    }

    private void render() {
        quadtree = new Quadtree(new Rectangle(0,0,1024,768), 0);
        quadtree.clear();

        if (ranOnce) {
            for (int i = 0; i < 1; i++) {
                addObjects();
                addEntity();
                ranOnce = false;
            }
        }

        ArrayList<Rectangle> possibleCollisions = new ArrayList<>();

        entity.move();
        entity.update();
        entity.checkEdges();
        entity.draw(gravCanvas.getGraphicsContext2D());

        quadtree.insert(entity);
        for (int i = 0; i < boxes.size(); i++) {
            Box b = boxes.get(i);
            b.draw(gc);
            b.checkEdges();
            b.move();
            b.update();
            quadtree.insert(b);
            allObjects.add(b);
        }

        for (int i = 0; i < boxes.size(); i++) {
            possibleCollisions.clear();
            quadtree.retrieve(possibleCollisions, entity);
            for (int j = 0; j < possibleCollisions.size(); j++) {
                if (entity.getBoundsInLocal().intersects(possibleCollisions.get(j).getBoundsInLocal())) {
                    if (possibleCollisions.get(j).getBoundsInLocal().contains(boxes.get(i).getBoundsInLocal())) {
                        boxes.remove(i);
                        break;
                    }
                }
            }
        }

        debug(true);
        sysInfo(true);
    }

    private void addEntity() {
        entity = new Entity(new PVector(480,370),50,75);
        entity.setVelocity(new PVector(random.nextInt(2) + 1, random.nextInt(2) + 1));
        entity.setAcceleration(new PVector(random.nextInt(5) + 1, random.nextInt(5) + 1));
    }

    private void addObjects() {
        random = new Random();
        for (int i = 0; i < 1000; i++) {
            box = new Box(new PVector((random.nextInt(1024) + 1), random.nextInt(768) + 1), 5, 5);
            box.setVelocity(new PVector(random.nextInt(2) + 1, random.nextInt(2) + 1));
            box.setAcceleration(new PVector(random.nextInt(5) + 1, random.nextInt(5) + 1));
            boxes.add(box);
        }
    }

    private void debug(boolean debug) {
        if (debug) {
            ArrayList<Rectangle> quadtrees = new ArrayList<>();
            quadtree.draw(quadtrees);

            for (Rectangle r : quadtrees) {
                gc = gravCanvas.getGraphicsContext2D();
                gc.setStroke(Color.RED);
                gc.strokeRect(r.getX(), r.getY(), r.getWidth(), r.getHeight());
            }
        }
    }

    private void sysInfo(boolean debug) {
        if (debug) {
            gc = gravCanvas.getGraphicsContext2D();
            gc.setStroke(Color.BLACK);
            gc.strokeText("Frame rate: " + frameRate, 800, 125);
        }
    }
}
