package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

class Point extends Circle {

    private PVector location;

    Point(PVector location, double r) {
        this.location = location;
        setCenterX(location.x);
        setCenterY(location.y);
        setRadius(r);
    }

    void draw(GraphicsContext gc) {
        gc.setStroke(Color.BLUE);
        gc.strokeOval(location.x, location.y, getRadius(), getRadius());
    }
}
