package sample;

/**
 * Based on https://gamedevelopment.tutsplus.com/tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374
 */

import javafx.scene.shape.Rectangle;
import java.util.List;
import java.util.ArrayList;

class Quadtree {

    private Rectangle bounds;
    private Quadtree[] nodes;
    private List<Rectangle> objects;

    private final int level;
    private final int NE = 0;
    private final int NW = 1;
    private final int SE = 2;
    private final int SW = 3;

    Quadtree(Rectangle bounds, int level) {
        this.bounds = bounds;
        this.level = level;
        nodes = new Quadtree[4];
        objects = new ArrayList<>();
    }

    void clear() {
        objects.clear();
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] != null) {
                nodes[i].clear();
                nodes[i] = null;
            }
        }
    }

    private void subdivide() {
        double x = bounds.getX();
        double y = bounds.getY();
        double w = bounds.getWidth() / 2;
        double h = bounds.getHeight() / 2;

        Quadtree northeast = new Quadtree(new Rectangle(x + w, y, w, h), level+1);
        nodes[NE] = northeast;
        Quadtree northwest = new Quadtree(new Rectangle(x, y, w, h), level+1);
        nodes[NW] = northwest;
        Quadtree southeast = new Quadtree(new Rectangle(x + w, y + h, w, h), level+1);
        nodes[SE] = southeast;
        Quadtree southwest = new Quadtree(new Rectangle(x, y + h, w, h), level+1);
        nodes[SW] = southwest;
    }

    private List<Integer> getIndex(Rectangle r) {

        List<Integer> indexes = new ArrayList<>();

        double verticalMidpoint = bounds.getX() + (bounds.getWidth() / 2);
        double horizontalMidpoint = bounds.getY() + (bounds.getHeight() / 2);

        boolean isNorthern = (r.getY() + (r.getHeight() / 2) <= horizontalMidpoint);
        boolean isSouthern = (r.getY() + (r.getHeight() / 2) >= horizontalMidpoint);
        boolean isBoth = (r.getY() <= horizontalMidpoint && r.getY() + r.getHeight() >= horizontalMidpoint);

        if(isBoth) {
            isNorthern = false;
            isSouthern = false;
        }

        if (r.getX() <= verticalMidpoint && (r.getX() + r.getWidth() >= verticalMidpoint)) {
            if (isNorthern) {
                indexes.add(NW);
                indexes.add(NE);
            }
            else if (isSouthern) {
                indexes.add(SE);
                indexes.add(SW);
            }
            else if (isBoth) {
                indexes.add(NW);
                indexes.add(NE);
                indexes.add(SW);
                indexes.add(SE);
            }
        }
        else if (r.getX() + (r.getWidth() / 2) <= verticalMidpoint) {
            if (isNorthern) {
                indexes.add(NW);
            }
            else if (isSouthern) {
                indexes.add(SW);
            }
            else if (isBoth) {
                indexes.add(NW);
                indexes.add(SW);
            }
        }
        else if (r.getX() + (r.getWidth() / 2) >= verticalMidpoint) {
            if (isNorthern) {
                indexes.add(NE);
            }
            else if (isSouthern) {
                indexes.add(SE);
            }
            else if (isBoth) {
                indexes.add(NE);
                indexes.add(SE);
            }
        }
        return indexes;
    }

    void insert(Rectangle r) {
        final int MAX_LEVELS = 5;
        final int MAX_OBJECTS = 1;

        if (nodes[NE] != null && nodes[NW] != null && nodes[SE] != null && nodes[SW] != null) {
            List<Integer> index = getIndex(r);
            for (int i = 0; i < index.size(); i++) {
                nodes[index.get(i)].insert(r);
            }
            return;
        }

        objects.add(r);

        if (objects.size() > MAX_OBJECTS && level < MAX_LEVELS) {
            subdivide();

            for (int i = 0; i < objects.size(); i++) {
                r = objects.get(i);
                List<Integer> indexes = getIndex(r);
                for (int j = 0; j < indexes.size(); j++) {
                    nodes[indexes.get(j)].insert(r);
                    objects.remove(r);
                }
            }
        }
    }

    void retrieve (List<Rectangle> returnObjects, Rectangle r) {
        if (nodes[NE] != null && nodes[NW] != null && nodes[SE] != null && nodes[SW] != null) {
            List<Integer> index = getIndex(r);
            for (int i = 0; i < index.size(); ++i) {
                nodes[index.get(i)].retrieve(returnObjects, r);
            }
        }
        returnObjects.addAll(objects);
    }

    void draw(ArrayList<Rectangle> quadtrees) {
        quadtrees.add(this.bounds);
        for (int i = 0; i < nodes.length; i++) {
            Quadtree node = nodes[i];
            if (node != null) {
                node.draw(quadtrees);
            }
        }
    }
}
